const fetch = require(`node-fetch`)
const auth = require(`@qnzl/auth`)

const { CLAIMS } = auth

const clientId = process.env.EXISTIO_CLIENT_ID

module.exports = async (req, res) => {
  const {
    authorization
  } = req.headers

  const {
    projectId = ''
  } = req.query

  const isTokenValid = auth.checkJWT(authorization, CLAIMS.existio.get, `watchers`, process.env.ISSUER)

  if (!isTokenValid) {
    return res.status(401).send()
  }

  const requestUrl = `https://exist.io/oauth2/authorize?response_type=code&client_id=${clientId}&redirect_uri=${process.env.EXISTIO_REDIRECT_URL}/api/request-token&scope=read+write+append`

  return res.send(requestUrl)
}

