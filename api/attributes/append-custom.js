const fetch = require('node-fetch')
const auth = require(`@qnzl/auth`)

const { CLAIMS } = auth

module.exports = async (req, res) => {
  const {
    authorization
  } = req.headers

  const isTokenValid = auth.checkJWT(authorization, CLAIMS.existio.update, `watchers`, process.env.ISSUER)

  if (!isTokenValid) {
    return res.status(401).send()
  }

  const headers = {
    'Authorization': `Bearer ${req.headers[`x-exist-access-token`]}`,
    'Content-Type': `application/json`,
  }

  const updateResp = await fetch(`https://exist.io/api/1/attributes/custom/append/`, {
      method: `POST`,
      headers,
      body: JSON.stringify(req.body)
    })

  const body = await updateResp.json()

  console.log(`updated attribute `, body)

  return res.json(body)
}

