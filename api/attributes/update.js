const fetch = require('node-fetch')
const auth = require(`@qnzl/auth`)

const { CLAIMS } = auth

module.exports = async (req, res) => {
  const {
    authorization
  } = req.headers

  const isTokenValid = auth.checkJWT(authorization, CLAIMS.existio.update, `watchers`, process.env.ISSUER)

  if (!isTokenValid) {
    return res.status(401).send()
  }

  const headers = {
    'Authorization': `Bearer ${req.headers[`x-exist-access-token`]}`,
    'Content-Type': `application/json`,
  }

  console.log(`acquiring attribute ${req.body.name}`)
  const acquireResp = await fetch(`https://exist.io/api/1/attributes/acquire/`, {
    method: `POST`,
    headers,
    body: JSON.stringify([{ name: req.body.name, active: true }])
  })

  const acq = await acquireResp.json()

  if (acquireResp.status !== 200) {
    console.log(`failed acquiring attribute`, acq)
    return res.status(500).send()
  }

  console.log(`updating ${req.body.name}`)
  req.body.value = req.body.value
  const updateResp = await fetch(`https://exist.io/api/1/attributes/update/`, {
      method: `POST`,
      headers,
      body: JSON.stringify(req.body)
    })

  const body = await updateResp.json()

  console.log(`updated attribute `, body)
  console.log(`releasing ${req.body.name}`)
  const releaseResp = await fetch(`https://exist.io/api/1/attributes/release/`, {
    method: `POST`,
    headers,
    body: JSON.stringify({ name: req.body.name })
  })

  console.log(`finished releasing`)

  return res.json(body)
}

