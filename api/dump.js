const moment = require('moment')
const fetch = require('node-fetch')
const flat = require('array.prototype.flat')
const auth = require(`@qnzl/auth`)

const { CLAIMS } = auth

flat.shim()

const delayedRequest = (url, windows, authorization, delay) => {
  return windows.map(([ start, windowLength ], index) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log(`Sending request for attributes starting at ${start} and running for ${windowLength}`)

        console.log("AUTHORIZATION:", authorization)
        return resolve(fetch(`https://exist.io/api/1/users/${process.env.EXISTIO_USERNAME}/${url}/?date_max=${start}&limit=${windowLength}`, {
          method: `GET`,
          headers: {
            'Authorization': `Bearer ${authorization}`
          }
        }))
      }, index * delay)
    })
  })
}

const getAttributes = async (attrPromises) => {
  return await Promise.all(attrPromises)
    .then((attributeResponses) => {
      return attributeResponses.reduce(async (map, response) => {
        let attributes = await response.json()

        if (!attributes || attributes.detail === `Not found.`) {
          return
        }

        console.log("ATTR:", attributes)

        attributes.forEach((attribute) => {
          const attributeName = attribute.attribute

          if (attributeName in map) {
            map[attributeName].values.concat(attribute.values)
          } else {
            map[attributeName] = attribute
          }
        })

        return map
      }, {})
    })
}

const getInsights = async (insightPromises) => {
  const insightResponses = await Promise.all(insightPromises)

  const moreInsightPromises = insightResponses.map(async (response) => {
    const { results } = await response.json()

    const mapped = results.map((result) => {
      const attributeName = result.type.attribute.name

      return { [attributeName]: result }
    })

    return mapped
  })

  let insights = await Promise.all(moreInsightPromises)

  return insights.flat()
}

const getAverages = async (avgsPromises) => {
  const averageResponses = await Promise.all(avgsPromises)

  const moreAveragePromises = averageResponses.map(async (response) => {
    const averages = await response.json()

    return averages.map((avg) => {
      const { attribute } = avg

      delete avg.attribute

      return { [attribute]: avg }
    })
  })

  let averages = await Promise.all(moreAveragePromises)

  averages = averages.flat()

  return Object.assign({}, ...averages)
}

const getCorrelations = async (correlationsPromises) => {
  const correlationResponses = await Promise.all(correlationsPromises)

  const correlationMap = {}
  const moreCorrelationPromises = correlationResponses.map(async (response) => {
    const correlations = await response.json()

    correlations.forEach((corr) => {
      const attribute1Name = corr.attribute
      const attribute2Name = corr.attribute2

      if (attribute1Name in correlationMap) {
        correlationMap[attribute1Name].push(corr)
      } else {
        correlationMap[attribute1Name] = [ corr ]
      }

      if (attribute2Name in correlationMap) {
        correlationMap[attribute2Name].push(corr)
      } else {
        correlationMap[attribute2Name] = [ corr ]
      }
    })
  })

  await Promise.all(moreCorrelationPromises)

  return correlationMap
}

module.exports = async (req, res) => {
  const {
    authorization
  } = req.headers

  const isTokenValid = auth.checkJWT(authorization, CLAIMS.existio.dump, `watchers`, process.env.ISSUER)

  if (!isTokenValid) {
    return res.status(401).send()
  }

  const { start = moment('2019-10-01'), end = moment('2019-12-31') } = req.query

  console.log(`get attributes`)

  const startDate = moment(start)
  const monthsDiff = moment(end).diff(moment(start), 'months')

  const windows = []
  for (let month = 0; month < monthsDiff + 1; month++) {
    const start = startDate.endOf('month').format('YYYY-MM-DD')
    const windowLength = moment(startDate).endOf('month').add(1, 'second').diff(startDate.startOf('month'), 'days')

    startDate.endOf('month').add(1, 'second')

    windows.push([ start, windowLength ])
  }

  console.log("HEADERS:", req.headers)
  const correlationsPromises = delayedRequest(`correlations/strongest`, windows, req.headers[`x-exist-access-token`], 3500)
  const insightsPromises = delayedRequest(`insights`, windows, req.headers[`x-exist-access-token`], 5000)
  const attrsPromises = delayedRequest(`attributes`, windows, req.headers[`x-exist-access-token`], 4000)
  const avgsPromises = delayedRequest(`averages`, windows, req.headers[`x-exist-access-token`], 6000)

  return res.send({
    attributes: await getAttributes(attrsPromises),
    insights: await getInsights(insightsPromises),
    averages: await getAverages(avgsPromises),
    correlations: await getCorrelations(correlationsPromises)
  })
}

