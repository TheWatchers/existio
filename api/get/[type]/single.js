const moment = require(`moment`)
const fetch = require('node-fetch')
const auth = require(`@qnzl/auth`)

const { CLAIMS } = auth

const buildQueryString = (query) => {
  const queryStrings = Object.keys(query).map((key) => {
    return `${key}=${query[key]}`
  })

  return queryStrings.join(`&`)
}

module.exports = async (req, res) => {
  console.log("REQ:", req.query)
  let { attribute = '', type, date } = req.query

  date = date.split(` `)[0] + `Z`

  const {
    authorization
  } = req.headers


  const isTokenValid = auth.checkJWT(authorization, CLAIMS.existio.get.single, `watchers`, process.env.ISSUER)

  if (!isTokenValid) {
    return res.status(401).send()
  }

  if (type !== `attributes`) {
    type = `${type}/attribute`
  }

  const url = `http://exist.io/api/1/users/${process.env.EXISTIO_USERNAME}/${type}/${attribute}`
  console.log("URR:", url, req.headers)
  const attrRes = await fetch(url, {
      method: `GET`,
      headers: {
          'Authorization': `Bearer ${req.headers[`x-exist-access-token`]}`
      }
    })

  try {
    const body = await attrRes.json()

    console.log("BODY:", body)
    console.log("DATE?", date)
    if (date) {
      body.results = body.results.filter((attr) => {
        console.log("ATTR:", moment(attr.date).format(), moment(date).format())
        return moment(attr.date).format(`YYYY-MM-DD`) === moment(date).format(`YYYY-MM-DD`)
      })
    }

    console.log(`returning attribute ${type}/${attribute} response`, body)

    return res.json(body)
  } catch (e) {
    console.log(`Error: `, e)
    return res.status(500).send()
  }
}



