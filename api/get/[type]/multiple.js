const fetch = require('node-fetch')
const auth = require(`@qnzl/auth`)

const { CLAIMS } = auth

module.exports = (req, res) => {
  let {
    type
  } = req.query

  const {
    authorization
  } = req.headers

  const isTokenValid = auth.checkJWT(authorization, CLAIMS.existio.get.multiple, `watchers`, process.env.ISSUER)

  if (!isTokenValid) {
    return res.status(401).send()
  }

  const validQueryVariables = [
    'limit',
    'page',
    'date_min',
    'date_max',
    'latest'
  ]

  const queryVariables = Object.keys(req.query).reduce((query, key) => {
    if (validQueryVariables.includes(key)) {
      query.push(`${key}=${req.query[key]}`)
    }

    return query
  }, [])

  const query = queryVariables.join('&')

  if (type === `correlations`) {
    type = `correlations/strongest`
  }

  fetch(`http://exist.io/api/1/users/${process.env.EXISTIO_USERNAME}/${type}?${query}`, {
      method: `GET`,
      headers: {
        'Authorization': `Bearer ${req.headers[`x-exist-access-token`]}`
      }
    })
    .then(async (response) => {
      try {
        const results = await response.json()

        console.log(`returning multiple ${type} response`)

        return res.json(results)
      } catch (e) {
        console.log(`Error: `, e)
        return res.status(500).send()
      }
    })
}



